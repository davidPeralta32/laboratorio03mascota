package com.example.laboratorio03mascota

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_saber_mascota.*

class SaberMascota : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_saber_mascota)

        val recibirMascota = intent.extras

        recibirMascota?.let { mascota ->
            val nombre = mascota.getString("KEY_Name") ?: "Desconocido"
            val edad = mascota.getString("KEY_Edad") ?: "Desconocido"
            val selectMascota = mascota.getString("KEY_Mascota") ?: "Desconocido"
            val vacuna1 = mascota.getString("KEY_Vacuna1") ?: "Vacuna1 no aplicada"
            val vacuna2 = mascota.getString("KEY_Vacuna2") ?: "Vacuna2 no aplicada"
            val vacuna3 = mascota.getString("KEY_Vacuna3") ?: "Vacuna3 no aplicada"

            when(selectMascota){
                "Perro" -> imgv_TipoMascota.setImageResource(R.drawable.dog)
                "Gato" -> imgv_TipoMascota.setImageResource(R.drawable.cat)
                "Conejo" -> imgv_TipoMascota.setImageResource(R.drawable.bunny)
                else -> imgv_TipoMascota.setImageResource(R.drawable.animal)
            }
            tvNombre.text = "Nombre: $nombre"
            tvEdad.text = "Edad: $edad"
            tvVacuna1.text = """
                $vacuna1
                $vacuna2
                $vacuna3
                """.trimIndent()
        }
    }
}