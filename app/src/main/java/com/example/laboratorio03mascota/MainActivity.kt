package com.example.laboratorio03mascota

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.CheckBox
import android.widget.EditText
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        btnEnviar.setOnClickListener {
            //Guardar Datos
            val name = edtNombre.text.toString()
            val edad = edtEdad.text.toString()
            val mascota = if(rbPerro.isChecked){
                "Perro"
            }else if(rbGato.isChecked){
                "Gato"
            }else{
                "Conejo"
            }
            //Validar Datos
            if(name.isEmpty()){
                toas("Ingresar el nombre de su mascota")
                return@setOnClickListener
            }

            if(edad.isEmpty()){
                toas("Ingresar la edad de su mascota")
                return@setOnClickListener
            }

            if(mascota.isEmpty()){
                toas("Seleccionar que animal es su mascota")
                return@setOnClickListener
            }


            val bundle = Bundle().apply {
                putString("KEY_Name",name)
                putString("KEY_Edad", edad)
                putString("KEY_Mascota",mascota.toString())
                if(verificarVacuna(chbVacuna1)) putString("KEY_Vacuna1", "Vacuna1 Aplicada")
                if(verificarVacuna(chbVacuna2)) putString("KEY_Vacuna2",  "Vacuna2 Aplicada")
                if(verificarVacuna(chbVacuna3)) putString("KEY_Vacuna3",  "Vacuna3 Aplicada")
            }

            val intent = Intent(this,SaberMascota::class.java).apply {
                this.putExtras(bundle)
            }

            startActivity(intent)

        }

    }

    fun verificarVacuna(checkBox: CheckBox):Boolean = checkBox.isChecked

    fun toas(mensaje:String):Unit{
        Toast.makeText(this, "$mensaje", Toast.LENGTH_SHORT).show()
    }

}